syntax_appserver
=========

Provisions:		
- Gunicorn + webapp docker-compose Service on port 8000


Requirements
------------
- docker
- docker-compose

Role Variables
--------------
N/A

Dependencies
------------
N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: ec2hosts
      roles:
     - { role: syntax_appserver }

License
-------

GNU

See also
------------------
N/A
